@extends('layouts.master')

@section('content')
<div class="col-sm-8 col-md-9 col-sm-push-4 col-md-push-3"><br/>
    <fieldset>
        <legend>
            View Photo
        </legend>

        @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
        @endif


        <table class="table table-striped">
            <tr>
                <td><b>Photo Id</b></td>
                <td><b>Name</b></td>
                <td><b>Caption</b></td>
                <td><b>Image</b></td>
                <td><b>Created By</b></td>
                <td><b>Created Date</b></td>
                <td><b>Operations</b></td>
            </tr>

            @foreach($records as $value)
            <tr>
                <td>{{ $value->id  }}</td>
                <td>{{ $value->name  }}</td>
                <td>{{ substr($value->description,0,100) }}...</td>
                <td>    
                    <?php $img = json_decode($value->image, true); ?>
                    @foreach($img['image'] as $val )

                    <img src="{{ asset('/original') }}/{{$val}}" width='50px' >

                    @endforeach 
                </td>
                <td>{{ $value->user->name  }}</td>
                <td>{{ $value->created_at  }}</td>
                <td><a href="{{ route('editphoto',$value->id) }} ">Edit</a> /<a href="{{ route('deletephoto',$value->id) }} " onclick="return confirm('Delete entry?')">Delete</a></td>

            </tr> 
            @endforeach
        </table>
    </fieldset>
    <div style="margin-left: 300px">
        {{ $records->links() }}
    </div>
</div>

@endsection