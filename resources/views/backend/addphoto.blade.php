@extends('layouts.master')

@section('content')
<form class="form-horizontal" method="POST" action="{{ route('storephoto') }}" enctype="multipart/form-data">
      <div class="col-sm-8 col-md-9 col-sm-push-4 col-md-push-3"><br/>
<fieldset>

<!-- Form Name -->
<legend>Add Photos</legend>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="view"></label>
  <div class="col-md-4">
     <a href="{{ route('viewphoto') }}" class="btn btn-primary">View All photos</a>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Image Name :</label>  
  <div class="col-md-4">
  <input id="name" name="name" type="text" placeholder="Enter Name" value="{{ old('name') }}" class="form-control input-md">
    
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="image">Upload Images :</label>
  <div class="col-md-4">
      <input id="image" name="image[]" class="input-file" type="file" required="required" multiple>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="description">Image Caption :</label>
  <div class="col-md-7">                     
    <textarea  class="textarea" style="resize: none;" name="description">{{ old('description') }}</textarea>
  </div>
</div>

<!-- Text input-->
 <input id="created" name="created_by" type="hidden"  value="{{ Auth::user()->id }}" class="form-control input-md">

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Add Photo</button>
  </div>
</div>

</fieldset>
      </div>
</form>
@endsection