
@extends('layouts.master')

@section('content')
<form class="form-horizontal" method="POST" action="{{ route('storepage') }}" enctype="multipart/form-data">
      <div class="col-sm-8 col-md-9 col-sm-push-4 col-md-push-3"><br/>
<fieldset>

<!-- Form Name -->
<legend>Add Page</legend>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="view"></label>
  <div class="col-md-4">
    <a href="{{ route('viewpage') }}" class="btn btn-primary">View All Pages</a>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Page Name :</label>  
  <div class="col-md-4">
  <input id="name" name="name" type="text" placeholder="Enter Name" value="{{ old('name') }}" class="form-control input-md">
    
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="select">Select Category :</label>
  <div class="col-md-4">
    <select id="select" name="cat_id" class="form-control">
      <option value="">SELECT CATEGORY</option>
      @foreach($category as $select):
      <option value="{{ $select->id }}" <?php if($select->id == old('cat_id')){ echo "selected='selected'";} ?> >{{ $select->name }}</option>
      @endforeach
    </select>
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="image">Page Image :</label>
  <div class="col-md-4">
      <input id="image" name="image" class="input-file" type="file" required="required">
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="description">Page Description :</label>
  <div class="col-md-7">                     
    <textarea  class="textarea" style="resize: none;" name="description">{{ old('description') }}</textarea>
  </div>
</div>

<!-- Text input-->
 <input id="created" name="created_by" type="hidden"  value="{{ Auth::user()->id }}" class="form-control input-md">

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Add Page</button>
  </div>
</div>

</fieldset>
      </div>
</form>
@endsection