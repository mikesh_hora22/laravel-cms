<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name' => 'required',
           'description' => 'required',
          'image[]'=>'image|mimes:jpeg,png,jpg,gif|max:100000',
          'created_by' => 'required'
        ];
    }
    
    public function messages() {
        parent::messages();
        
        return [
            'name.required' => 'Image Name Field is Required.',
            'description.required' => 'Image Caption Field is Required.',
            'created_by.required' => 'Created By Field is Required.'
        ];
    }
}
