<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
use App\Model\pages;
use App\Model\category;

use DB;

class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
         $records = pages::orderBy('id','desc')->paginate(5);

        return view('backend.viewpage', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $category = DB::table('category')->select('id', 'name')->get();
        
           return view('backend.addpage', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request) {
        $img = $request->file('image');

        $image = $this->image_upload($img);

        $input = $request->except('image');
        $input['image'] = $image;

        pages::create($input);

        Session::flash('flash_message', 'Page successfully added!!');

        return redirect()->route('viewpage');
    }

    public function image_upload(\Illuminate\Http\UploadedFile $img) {

        $profileimage = $img;

        $filename = time() . '.' . $profileimage->getClientOriginalExtension();

        $destination = public_path('/original');


        $profileimage->move("$destination", "$filename");


        return $filename;
    }

  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $category = DB::table('category')->select('id', 'name')->get();

        $result = pages::findOrFail($id);

        return view('backend.editpage', compact('category', 'result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $result = pages::findOrFail($id);

        $update = $request->except('image');

        $img = $request->file('image');

        if ($img == '') {
            $result = pages::findOrFail($id);
            $image = $result->image;
        } else {

            $image = $this->image_upload($img);
        }

        $update['image'] = $image;

        $result->fill($update)->save();

        Session::flash('flash_message', 'Page Updated Sucessfully!');

        return redirect()->route('viewpage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $result = pages::findOrFail($id);

        $result->where('id', $id)->delete();

        Session::flash('flash_message', 'Page Deleted Sucessfully!');

        return redirect()->route('viewcategory');
    }

}
