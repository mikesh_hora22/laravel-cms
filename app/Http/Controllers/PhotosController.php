<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\PhotoRequest;
use App\Model\photos;

class PhotosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $records = photos::orderBy('id','desc')->paginate(5);
        return view('backend.viewphoto', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('backend.addphoto');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoRequest $request) {
        $input = $request->except('image');


        $img = $request->file('image');

        $img_count = count($img);

        $upload_count = 0;

        foreach ($img as $image) {
            $destinationPath = public_path('/original');
            $image->move($destinationPath, $image->getClientOriginalName());
            $images['image'][]= $image->getClientOriginalName();
            $upload_count ++;
        }

        if ($upload_count == $img_count) {
            $value = json_encode($images);

            $input['image'] = $value;
            photos::create($input);
            Session::flash('flash_message', 'Photo successfully added!!');

            return redirect()->route('viewphoto');
        }
    }

 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $result = photos::findOrFail($id);

        return view('backend.editphoto', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhotoRequest $request, $id) {
        $result = photos::findOrFail($id);

        $update = $request->except('image');

        $img = $request->file('image');

        if ($img == '') {
            $result = photos::findOrFail($id);
            $image = $result->image;
        } else {

            $img_count = count($img);

            $upload_count = 0;

            foreach ($img as $image) {
                $destinationPath = public_path('/original');
                $image->move($destinationPath, $image->getClientOriginalName());
                $images['image'][] = $image->getClientOriginalName();
                $upload_count ++;
            }
            if ($upload_count == $img_count) {
                $value = json_encode($images);

                $update['image'] = $value;
            }
        }

        $result->fill($update)->save();

        Session::flash('flash_message', 'Photo Updated Sucessfully!');

        return redirect()->route('viewphoto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $result = photos::findOrFail($id);

        $result->where('id', $id)->delete();

        Session::flash('flash_message', 'Photo Deleted Sucessfully!');

        return redirect()->route('viewphoto');
    }

}
