<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Model\category;
use App\Model\pages;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $records = category::orderBy('id','desc')->paginate(5);
//        $records = DB::table('category')
//                ->join('pages', 'category.id', '=', 'pages.cat_id')
//                ->select('category.*',DB::raw('count(pages.cat_id )as pcount'))
//                ->groupBy('cat_id')
//                ->orderBy('category.id', 'desc')
//                ->get();
//        var_dump($records);
//        exit;
        
//        $value = pages::groupBy('cat_id')->count('cat_id');
//        print_r($value);exit;
        return view('backend.viewcategory', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.addcategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request) {

        $img = $request->file('image');

        $image = $this->image_upload($img);

        $input = $request->except('image');
        $input['image'] = $image;

//          print_r($input);exit;

        category::create($input);

        Session::flash('flash_message', 'Category successfully added!!');

        return redirect()->route('viewcategory');
    }

    public function image_upload(\Illuminate\Http\UploadedFile $img) {

        $profileimage = $img;

        $filename = time() . '.' . $profileimage->getClientOriginalExtension();

        $destination = public_path('/original');


        $profileimage->move("$destination", "$filename");


        return $filename;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $result = category::findOrFail($id);

        return view('backend.editcategory', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CategoryRequest $request) {

        $result = category::findOrFail($id);

        $update = $request->except('image');

        $img = $request->file('image');


        if ($img == '') {

            $result = category::findOrFail($id);

            $image = $result->image;
        } else {
            $image = $this->image_upload($img);
        }

        $update['image'] = $image;

        $result->fill($update)->save();

        Session::flash('flash_message', 'Category Updated Sucessfully!');

        return redirect()->route('viewcategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $result = category::findOrFail($id);

        if (DB::table('pages')->find($id, $column = ['cat_id'])) {
            Session::flash('flash_message', 'Category Has Pages Cannot Delete!!');  
            return redirect()->route('viewcategory');
        } else {
            $result->where('id', $id)->delete();
            Session::flash('flash_message', 'Category Deleted Sucessfully!');
            return redirect()->route('viewcategory');
        }
    }

}
