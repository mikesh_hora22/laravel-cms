<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_users=count(DB::table('users')->get());
        $total_category=count(DB::table('category')->get());
        $total_page=count(DB::table('pages')->get());
        $total_photo=count(DB::table('photos')->get());
            
        return view('backend.dashboard',compact('total_users','total_category','total_page','total_photo'));
    }
}
