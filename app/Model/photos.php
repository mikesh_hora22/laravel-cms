<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class photos extends Model
{
 
     protected $table = 'photos';


    protected $fillable =[
        'name',
        'description',
        'image',
        'created_by'     
      ];
    
     public function user(){
        return $this->belongsTo('App\user','created_by');
    }
}
