<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
     protected $table = 'category';


    protected $fillable =[
        'name',
        'description',
        'image',
        'created_by'     
      ];
    
    public function user(){
        
        return $this->belongsTo('App\user','created_by');
    }
    
    public function pages(){
        return $this->hasMany('App\Model\pages','cat_id','id');
    }
}
