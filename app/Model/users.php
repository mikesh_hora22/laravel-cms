<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{  protected $table = 'users';


    protected $fillable =[
        'name',
        'email',
        'password',
        
    ];
    
      public function category(){
        return $this->hasMany('App\Model\category','created_by');
    }
    
    public function pages(){
        return $this->hasMany('App\Model\pages','created_by');
    }
    
    public function photos(){
        return $this->hasMany('App\Model\photos','created_by');
    }
    
}
