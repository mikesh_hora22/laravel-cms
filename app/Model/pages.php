<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class pages extends Model
{
     protected $table = 'pages';


    protected $fillable =[
        'name',
        'cat_id',
        'image',
        'description',
        'created_by'     
      ];
    
     public function user(){
        
        return $this->belongsTo('App\user','created_by');
    }
    
    public function category(){
        return $this->belongsTo('App\Model\category','cat_id');
    }
}
