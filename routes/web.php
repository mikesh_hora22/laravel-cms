<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'HomeController@index')->name('dashboard');
    Route::get('/category', 'CategoryController@index')->name('viewcategory');
    Route::get('/add-category', 'CategoryController@create')->name('addcategory');
    Route::post('/add-category', 'CategoryController@store')->name('storecategory');
    Route::get('/edit-category/{id}', 'CategoryController@edit')->name('editcategory');
    Route::post('/update-category/{id}', 'CategoryController@update')->name('updatecategory');
    Route::get('/delete-category/{id}', 'CategoryController@destroy')->name('deletecategory');

    Route::get('/page', 'PagesController@index')->name('viewpage');
    Route::get('/add-page', 'PagesController@create')->name('addpage');
    Route::post('/add-page', 'PagesController@store')->name('storepage');
    Route::get('/edit-page/{id}', 'PagesController@edit')->name('editpage');
    Route::post('/update-page/{id}', 'PagesController@update')->name('updatepage');
    Route::get('/delete-page/{id}', 'PagesController@destroy')->name('deletepage');

    Route::get('/photo', 'photosController@index')->name('viewphoto');
    Route::get('/add-photo', 'PhotosController@create')->name('addphoto');
    Route::post('/add-photo', 'PhotosController@store')->name('storephoto');
    Route::get('/edit-photo/{id}', 'PhotosController@edit')->name('editphoto');
    Route::post('/update-photo/{id}', 'PhotosController@update')->name('updatephoto');
    Route::get('/delete-photo/{id}', 'PhotosController@destroy')->name('deletephoto');
});


